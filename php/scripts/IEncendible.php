<?php
include '../scripts/IInstalable.php';

interface IEncendible extends IInstalable {
  public function encender();
  public function apagar();
}
?>
